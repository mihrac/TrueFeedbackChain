class Profile < ApplicationRecord
  belongs_to :profilable, polymorphic: true, optional: true
  enum gender: ["Female", "Male"]
  enum lang: ["English", "Deutsche", "Francais", "Español", "Chinese", "Korean", "Turkish"]
#attr_accessor :country_code, :state_code
validates :country_code, :state_code, :birthday, presence: true, if: :profilable_type?
validates :country_code, :state_code, presence: true, unless: :profilable_type?
validates :max_age, presence: true
validates :min_age, presence: true



def profilable_type?
self.profilable_type == "User"
	
end





end
