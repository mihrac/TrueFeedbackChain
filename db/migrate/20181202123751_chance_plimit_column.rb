class ChancePlimitColumn < ActiveRecord::Migration[5.2]
  def change
    change_column :surveys, :plimit, :integer, default: 1000
  end
end
